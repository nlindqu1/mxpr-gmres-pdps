\documentclass[11pt]{article}

\usepackage{mathptmx}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{siunitx}
\usepackage[dvipsnames,svgnames,x11names]{xcolor}
\usepackage{todonotes}
\usepackage{url}

\usepackage[margin=1in]{geometry}

\newcommand{\consider}[1]{\todo[inline, color=DeepSkyBlue]{#1}}
\newcommand{\wontdo}[1]{\todo[inline, color=LightPink]{#1}}
\newcommand{\wip}[1]{\todo[inline, color=yellow!75]{#1}}
\newcommand{\done}[1]{\todo[inline, color=LightGreen]{#1}}

\setlength{\parskip}{1ex}

\begin{document}
%\wontdo{Comments that were considered but didn't fit in the scope of the
%revision}
%\wip{Comments that have been partially addressed}
%\done{Comments that have been addressed}

The reviews provided some excellent suggestions for improving our paper.
We were able to address most of these comments.
However, there were some improvements to the experimental results that
did not make it into the re-submission, due to the revision deadline.
%
We address the individual comments below, with colors indicating how
well we were able to address the issue.


\bigskip

\bigskip

\done{(R1) This paper only tests the mixed-precision GMRES on
limited cases from SuiteSparse collection but not on problems from
real-world scenarios. It would be much better to test the algorithm on
problems like PDE and evaluate the sustained performance gain.}

Some of the SuiteSparse problems that we used for our experiments
originate from the real world such as PDE solvers on scientific
simulation models. In order to testing the performance as would have
been observed by a full PDE solver we used the provided RHS vectors
whenever they were available in the collection.  Please see below for
additional information on the new performance results.

\done{(R1) Experiments in the paper draw random x to test the algorithm.
However, it should be noted that for many real scenarios the solution is
not random but with structures. Take the 1-D central differential
Laplacian $[-1, 2, -1]$ as an example of $A$. When entries of x are sampled
from $U[0, 1)$ i.i.d., entries of x is of $O(1)$ and entries of
$b = A x$ is also of $O(1)$. However, if $x$ represents a smooth
function on the grid, then entries of $b = A x$ is of $O(h^2)$ while $x$
is of order $O(1)$. In this case, because of the catastrophic
cancellation, it may require much more bits for $x$ to reach a specific
relative error or residual. As a result, other choices of solutions
should be investigated, or at least for a model problem with
differential stencils.}

We have redone the results in order to use the right-hand-sides provided
by the SuiteSparse collection for some problems.


\wip{(R1) Comparison with only low-precision arithmetic is absent, which
is critical to justify the necessity of the algorithm. Since there are
also several other versions of mixed-precision GMRES, comparison between
these algorithms is also important to show a real advantage.}

We've added comparison against a strictly single-precision implementation.
This should clarify the advantages (reduced data size and bandwidth
demand) and disadvantages (limited precision) without taking too much
space away from the mixed-precision contribution.

In our experience, Compressed Basis (CB) GMRES would certainly be useful
to compare against as a target.
CB GMRES requires mixed-precision kernels to implement when porting it
to our experimental setup for an even-level comparison. In our
experience, getting comparable performance out of method-specific kernel
was taking longer than we initially expected and we couldn't make the
cut-off date for the revised manuscript.  Instead, we did add more
details of the specific advantages and disadvantage of the approaches to
help the reader justify and give context for using our approach and
under what circumstances it may prove useful.


\wontdo{(R1) The algorithm is tested on some simple preconditioners
which do not include many popular ones. For many cases when the number
of iterations is large, complicated preconditioners will be introduced
and will take a lot of time.}

This paper may be considered an initial work of integrating
mixed-precision approach into iterative solvers for PDE-based
applications. We focused on assessing the benefits in the scenario when
the preconditioner is simple and the overheads originate from the other
components of the iteration process. Please also see the answer to the
next question.

\wontdo{(R1) The theorem in the paper only focuses on GMRES without
preconditioner. However, in the cases with preconditioners, the
preconditioners are evaluated in high precision and type casts are
usually required. In this way, the map of applying the preconditioner is
not linear and constant, which may hurts the convergence of Krylov
methods. It would be much better if this can be covered, either by
carrying out analysis with preconditioners or by testing on more
complicated preconditioners.}

We considered an implementation of a ``complicated'' preconditioner
inside of our existing setup however it was infeasible in the timeframe
for the revisions. Furthermore, as indicated by the reviewer, the
theoretical analysis is also non-trivial.
Dealing with non-constant preconditioner would require switching the
underlying iteration scheme to FGMRES which alters the core assumptions
of this work and is left as future work.

\done{(R1) The paper reports 65\% and 54\% average speedups for CGSR.
However, on V100 the computational capability of fp64 and fp32 is 1:2.}

We added discussion to the un-preconditioned and Jacobi-preconditioned
sections in order to explain how the better speedups that occur when the
Krylov basis is large in comparison to the per-row nonzero count of the
matrix. For those specific matrices, the transfer of the index array is
only a minor part of the runtime.

\done{(R1) According to Table 1 and 2, it seems that CGSR with low
precision may converge faster than full precision ones. I am wondering
how this is possible and it would be much better to explain for a little
bit.}

We were able to confirm this effect but couldn't determine the precise
circumstances and mechanism that allowed for the improved convergence
behavior.  We added a possible explanation that suggests the reduction
in the precision may perturb the Krylov basis to contain more components
that correspond to the final solution.


\done{(R1) The description of the algorithm in section 3 intertwines
with the theorem and results of analysis. It is confusing which part of
the algorithm runs in low precision and which in high precision.
Although such description hides somewhere in the introduction section,
it is best to annotate the precision settings in the algorithm listing.}

The algorithm now includes extra column to indicate the floating-point
format used on each line (where applicable.)


\done{(R1) It is also helpful to note which part of the algorithm runs
on CPU and which on GPU.}

We clarified in the text that all of the computation was done on the GPU
and only control flow dispatch is handled by the CPU.


\done{(R2) The discussion on the number of iterations is quite
interesting. But providing only the number of iterations might be still
incomplete. Since there is already a theorem that tries to build the
difference gap between a finite-precision solution and an
exact-precision solution, it would be good to provide some numercial
analysis regarding the iterative convergence process.}

We added a note in the introduction section that points the reader to
our previous work on experiments with the convergence behavior when
various parts of GMRES are stored in single precision. Our current
scheme is based on these experiments.

\done{(R2) In the current discussion about the number of iterations, it seems quite clear that the cases where the performance benefits were not achieved mainly suffer from an extra restart. Some more numerical discussions about this would be helpful.}

We added more discussion on the performance implications of restarting.


\done{(R2) Current and future GPUs and other kinds of accelerators
already support half-precision arithmetic. There are also existing
papers discussing the potential of using 16-bit units in scientific
computing. The authors might want to answer the question that why
half-precision is not included in the design of the algorithm and the
discussion.}

This work is based on previous work that only tested the convergence of
mixing single and double precisions.  With multiple half-precision
variants and extensions, including IEEE 764-2019 FP16 or Google BFloat16
or NVIDIA TF32 of 19 bits combined, are left out due to space
constraints and will be the subject of investigation in future work.

\done{(R2) The use of precision in the algorithm seems to be kind of
fixed. For an iterative process, should we consider the use of different
precisions in different iterations?}

We added extra discussion in the previous work section about of the
trade-offs when changing the precision as iterations progresses, which
was originally proposed by Gratton et al. [13]


\done{(R2) The current survey on related precision seems to be slightly
concentrated to mixed-precision techniques applied to GMRES. Other
similar solvers should also be related. It would be good to have a more
general discussion regarding various techniques that could be applied to
different parts of an iterative solver (preconditioner, the solving
process, the storage in memory, the compute, etc.), what are the
benefits, the constraints, and the impacts to the accuracy.}

We added more discussion with examples of previous mixed-precision work
for other iterative solvers and additional mentions of work on inexact
GMRES.

\wip{(R2) Also, since the authors already mention a number of existing
work from the same group, it could be a good idea to compare this new
work against the previous work in the group, to demonstrate the value of
the new method.}

%(I think the second reviewer is referring to some of Hartwig's work.)

In our experience, Compressed Basis (CB) GMRES would certainly be useful
to compare against as a target.
CB GMRES requires mixed-precision kernels to implement when porting it
to our experimental setup for an even-level comparison. In our
experience, getting comparable performance out of method-specific kernel
was taking longer than we initially expected and we couldn't make the
cut-off date for the revised manuscript.  Instead, we did add more
details of the specific advantages and disadvantage of the approaches to
help the reader justify and give context for using our approach and
under what circumstances it may prove useful.


\done{(R3) It is mentioned at the beginning of Section 5 that their
algorithm was implemented using Kokkos, but it is unclear what effect
this had on the performance. It would be better from a reproducibility
perspective to clarify whether an implementation in Kokkos is a
requirement or a minor detail that does not affect the performance.}

We added a note that the Kokkos performance is expected to be similar to
native CUDA performance while offering additional portability benefits
beyond GPU accelerators from a single vendor.


\done{(R4) Figure 1, Figure2 and Figure3 are difficult to read, the graphs either should be made bigger or in colour.}

We increased the size of the graphs and switched from normalized-runtime to speedup (which should make it easier to see trends and relative performance).

\end{document}
